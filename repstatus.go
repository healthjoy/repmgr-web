package main


import (
    "io"
    "log"
    "regexp"
    "net/http"
    "os/exec"
    "os"
    "strings"
)

// hello world, the web server
func HelloServer(w http.ResponseWriter, req *http.Request) {
	out, _ := exec.Command("/usr/bin/repmgr", "-f", "/etc/repmgr/repmgr.conf", "cluster", "show").Output()
	lines := strings.Split(string(out), "\n")
        hostname, _ := os.Hostname()
	re := regexp.MustCompile(`\..*`)
        hostname = re.ReplaceAllString(hostname, "")
        for _, line := range lines {
        	if (strings.Contains(line, hostname)) {
			if strings.Contains(line, "standby") {
				io.WriteString(w, "STANDBY")
			} else if strings.Contains(line, "master") {
				io.WriteString(w, "MASTER")
			} else {
				w.WriteHeader(500)
				io.WriteString(w, "ERROR")
			}
			return
		}
	}
	w.WriteHeader(500)
	io.WriteString(w, "ERROR")
}

func main() {
	http.HandleFunc("/", HelloServer)
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
